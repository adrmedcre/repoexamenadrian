package org.iesam.adrianmedico.test;

import static org.junit.Assert.fail;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;


import org.iesam.adrianmedico.Product;
import org.iesam.adrianmedico.ProductNotFoundException;
import org.iesam.adrianmedico.ShoppingCartAdrianMedico;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.rules.ExpectedException;

class ShoppingCardTest {
	public ShoppingCartAdrianMedico _bookCart;
	public Product _defaultBook;
	
	@BeforeEach
	public void prepararTest() {
		this._bookCart = new ShoppingCartAdrianMedico();

		this._defaultBook = new Product("Extreme Programming", 23.95);
		_bookCart.addItem(_defaultBook);
	}
	
	@Test
	public void testCompruebaVacio() {
		_bookCart.empty();
		Assert.assertTrue(_bookCart.isEmpty());
		//
	}
	@Test
	public void testAñadirProducto() {
		Product libro = new Product("Clean code", 1.05);
		_bookCart.addItem(libro);
	
		Assert.assertEquals(2, _bookCart.getItemCount());
		Assert.assertEquals(25, (int) _bookCart.getBalance()); //delta = precision
		//
	}
	
	
	@Test
	public void testProductoNoExistente() {
		try {
			ExpectedException exception = ExpectedException.none();
			exception.expect(ProductNotFoundException.class);
			Product book = new Product("book", 15);
			_bookCart.removeItem(book);
			fail();
		} catch (ProductNotFoundException exception) {
			
			
		}
	}
	
	@Test
	public void testQuitarProducto() {
		try {
			//me da error el metodo temoveItem ???
			_bookCart.removeItem(_defaultBook);

			
			Assert.assertEquals(0, (int)_bookCart.getBalance());
//			int count = _bookCart.getItemCount();
			Assert.assertEquals(0, _bookCart.getItemCount());
		} catch (ProductNotFoundException e) {
			e.getMessage();
		}
	}
	@AfterEach
	public void limpiarTest() {
		_bookCart = null;
	}

}
